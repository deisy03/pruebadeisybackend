package com.adsi.rent.web.rest;

import com.adsi.rent.domain.Rent;
import com.adsi.rent.service.IRentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RentResource {

    @Autowired
    IRentService rentService;

    @GetMapping("/rent")
    public Iterable<Rent>read(){
        return rentService.read();
    }

    @PostMapping("/rent")
    public Rent create (@RequestBody Rent rent){
        return rentService.create(rent);
    }
}
