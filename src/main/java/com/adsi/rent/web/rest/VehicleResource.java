package com.adsi.rent.web.rest;

import com.adsi.rent.domain.Rent;
import com.adsi.rent.domain.Vehicle;
import com.adsi.rent.service.IRentService;
import com.adsi.rent.service.IVehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class VehicleResource {

    @Autowired
    IVehicleService vehicleService;

    @GetMapping("/vehicle")
    public Iterable<Vehicle>read(){
        return vehicleService.read();
    }

    @PostMapping("/vehicle")
    public Vehicle create (@RequestBody Vehicle vehicle){
        return vehicleService.create(vehicle);
    }
}