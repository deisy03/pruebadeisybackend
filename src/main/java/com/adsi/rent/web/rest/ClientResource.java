package com.adsi.rent.web.rest;

import com.adsi.rent.domain.Client;
import com.adsi.rent.service.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ClientResource {

    @Autowired
    IClientService clientService;

    @GetMapping("/client")
    public Iterable<Client>read(){
        return clientService.read();
    }

    @PostMapping("/user")
    public Client create (@RequestBody Client client){
        return clientService.create(client);
    }
}
