package com.adsi.rent.service;

import com.adsi.rent.domain.Client;
import com.adsi.rent.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IClientServiceImp implements IClientService {

    @Autowired
    ClientRepository clientRepository;

    @Override
    public Iterable<Client> read() {
        return clientRepository.findAll();
    }

    @Override
    public Client create(Client client) {
        return clientRepository.save(client);
    }
}
