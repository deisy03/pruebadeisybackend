package com.adsi.rent.service;

import com.adsi.rent.domain.Rent;

public interface IRentService {

    public Iterable<Rent>read();
    public Rent create(Rent rent);
}
