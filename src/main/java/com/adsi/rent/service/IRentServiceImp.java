package com.adsi.rent.service;

import com.adsi.rent.domain.Rent;
import com.adsi.rent.repository.RentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IRentServiceImp implements IRentService {

    @Autowired
    RentRepository rentRepository;

    @Override
    public Iterable<Rent> read() {
        return rentRepository.findAll();
    }

    @Override
    public Rent create(Rent rent) {
        return rentRepository.save(rent);
    }
}
