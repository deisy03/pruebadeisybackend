package com.adsi.rent.service;

import com.adsi.rent.domain.Vehicle;
import com.adsi.rent.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IVehicleServiceImp implements IVehicleService {

    @Autowired
    VehicleRepository vehicleRepository;

    @Override
    public Iterable<Vehicle> read() {
        return vehicleRepository.findAll();
    }

    @Override
    public Vehicle create(Vehicle vehicle) {
        return vehicleRepository.save(vehicle);
    }
}
