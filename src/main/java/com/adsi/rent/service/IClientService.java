package com.adsi.rent.service;

import com.adsi.rent.domain.Client;

public interface IClientService {

    public Iterable<Client>read();
    public Client create(Client client);
}
