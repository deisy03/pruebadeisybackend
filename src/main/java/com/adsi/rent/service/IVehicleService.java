package com.adsi.rent.service;

import com.adsi.rent.domain.Vehicle;

public interface IVehicleService {

    public Iterable<Vehicle>read();
    public Vehicle create(Vehicle vehicle);
}
